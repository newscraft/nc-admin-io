const mongoose = require('mongoose');
const configAuth = require('./config/auth');

var uri = configAuth.mongodbHost;

var connection = mongoose.createConnection(uri, { dbName: 'nc-admin', useNewUrlParser: true });
console.log(' exec '+ mongoose.connection.readyState);
// var connection = mongoose.connect(uri, { dbName: 'nc_assets', useNewUrlParser: true }, (err) => {
// 	if (err) throw err;
// 	console.log('MongoDB asset connected!')
// });

module.exports = connection;