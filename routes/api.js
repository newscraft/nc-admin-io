var express = require('express');
var _ = require('underscore');
// var moment = require('moment');
const moment = require('moment-timezone');
var router = express.Router();
var axios = require('axios');
var useragent = require('useragent');

var dbMySQL = require('../dbMySQL');

const { getUserAll, getUsers, getPermissions, getPublishers } = require("../functions");
const { getAdmin } = require("../functions/api");

router.post('/purge', async function(req, res, next) {

	var publisher_id = req.headers['x-pub-id'];
	if ( !publisher_id ) return res.status(401).send({ status: false, message: 'Invalid Publisher ID' });

	var data = req.body;
	var results = await purgekey(data);

	if( _.contains(results, 'ok') ){ res.status(200).send(results); }else{ res.status(400).send(results); }

});

router.get('/admin/:id?', async function(req, res, next) {
	var id = req.params.id ? req.params.id : ''
	const admin = await getAdmin(id)
	
	Promise.all(admin).then(response => {
		res.status(200).send(response)
	}).catch(err => {
		console.log(err)
		res.status(500).send(err)
	})
	
});

router.get('/userAll/:sanitize?', async function(req, res, next) {
	var key = req.params.sanitize ? req.params.sanitize : ''
	const users = await getUserAll(false, false, key);
	res.status(200).send(users);
});

router.get('/user', async function(req, res, next) {

	var pub_id = req.cookies['pub_id'];
	if( !pub_id ){ res.status(404).send(); }

	const users = await getUsers('', pub_id);
	const permissions = await getPermissions();	

	res.status(200).send(users);

});

router.get('/log/:entity/:key?', async function(req, res, next) {

	var publisher_id = req.headers['x-pub-id'];
	// if ( !publisher_id ) return res.status(401).send({ status: false, message: 'Invalid Publisher ID' });
	var property_id = req.headers['x-property-id']

	var key = req.params.key;
	var entity = req.params.entity;
	
	if( entity == 'asset' ){
		var master = await assetLogs(publisher_id, entity, key);
	}else if ( entity == 'story' ) {
		var master = await storyLogs(publisher_id, entity, key);
	}else if ( entity == 'activity' ) {
		var master = await activityLogs(publisher_id, property_id, entity, key);
	}else{
		res.status(404).send({ status: false, message: 'path not found' });
	}

	res.status(200).send(master);

});


const assetLogs = async (publisher_id, entity, key) => {
	
	return new Promise( async (resolve, reject) => {

		if( key ){ 
			var uri = `https://video-sync.newscraft.io/log/${ key }`;
		}else{
			var uri = `https://video-sync.newscraft.io/log`;
		}
		
		var option = {
			method: 'GET',
			headers: {
				'x-pub-id': `${ publisher_id }`,
				'x-api-key': '1173e4fe-4275-4300-b53c-b8bfb7f053da',
				'Content-Type': 'application/json'
			}
		}

		try{

			var result = await axios(uri, option);

			if(!key){

				// var total = result.data.total;
				// var totalNotFiltered = result.data.totalNotFiltered;
				var total = result.data.length;
				var totalNotFiltered = result.data.length;

				var results = result.data;
				var data = results.map(function(rows){
					var unixDT = moment(rows.date_upload);
					var humanDT = unixDT.tz('Asia/Kuala_Lumpur').format("YYYY-MM-DD HH:mm");
					var obj = {}
					obj.id = rows.id;
					obj.date = humanDT;
					obj.user = rows.upload_by || '-';
					obj.event = 'upload';
					obj.file_name = rows.file_original_name;
					obj.content_type = rows['Content-Type'];
					obj.uri = rows.uri_origin;
					return obj;
				});

				var payload = {}
				payload.total = total;
				payload.totalNotFiltered = totalNotFiltered;
				payload.rows = data;

				resolve(payload);

			}else{
				resolve(result.data);
			}
			

		} catch (error) {
			console.log(error)
			reject(error);
		}

	});

}

const storyLogs = (publisher_id, entity, key) => {

	return new Promise( async (resolve, reject) => {

		if( key ){
			var uri = `http://52.221.245.110:4100/api/v1/log/story/${ key }`;	
		}else{
			var uri = `http://52.221.245.110:4100/api/v1/log/story`;		
		}

		var option = {
			method: 'GET',
			headers: {
				'x-pub-id': `${ publisher_id }`,
				'x-api-key': '1173e4fe-4275-4300-b53c-b8bfb7f053da',
				'Content-Type': 'application/json'
			}
		}

		try{

			var result = await axios(uri, option);

			if(!key){

				// var total = result.data.total;
				// var totalNotFiltered = result.data.totalNotFiltered;
				var total = result.data.length;
				var totalNotFiltered = result.data.length;				

				var data = await getLogDetails(publisher_id, result.data);

				var payload = {}
				payload.total = total;
				payload.totalNotFiltered = totalNotFiltered;
				payload.rows = data;

				resolve(payload);

			}else{
				resolve(result.data);
			}
			

		} catch (error) {
			reject(error);
		}

	});
}

const activityLogs = (publisher_id, property_id, entity, key) => {
	return new Promise( async (resolve, reject) => {

		try{
			
			var uri = `https://hlnqv8nybj.execute-api.ap-southeast-1.amazonaws.com/prod/act`

			var params = {}
			params.pub_id = publisher_id
			key ? params.user = key : ''
			var options = {
				method: 'GET',
				params: params
			}

			var result = await axios(uri, options);
			var rows = result.data ? result.data : [];

			var payload = []
			for(var i in rows){
				var time_human = ''
				moment.unix(rows[i].time, "X").isSame(moment(), 'day') ?
					time_human = moment.unix(rows[i].time, "X").utcOffset("+08:00").format("h:mm:ss A") :
					time_human = moment.unix(rows[i].time, "X").utcOffset("+08:00").format("lll")
				
				var agent = useragent.parse(rows[i].browser);

				var obj = {};
				obj.user = rows[i].user;
				obj.user_color = pastel_colour(rows[i].user);
				obj.time = moment.unix(rows[i].time, "X").format("lll");
				obj.time_human = time_human;
				obj.browser = rows[i].browser;
				obj.browser_human = agent.toString();
				obj.path = rows[i].uri;

				payload.push(obj)	 	
			}

			resolve(payload);
			// var pl_list = {
			// 	'key' : key,
			// 	'type' : 'activity',
			// 	'data' : {
			// 		'rows' : payload
			// 	}
			// }
			// resolve(pl_list);

		} catch (error) {
			reject(error);
		}


	});
}

function pastel_colour(input_str) {

    //TODO: adjust base colour values below based on theme
    var baseRed = 128;
    var baseGreen = 128;
    var baseBlue = 128;

    //lazy seeded random hack to get values from 0 - 256
    //for seed just take bitwise XOR of first two chars
    var seed = input_str.charCodeAt(0) ^ input_str.charCodeAt(1);
    var rand_1 = Math.abs((Math.sin(seed++) * 10000)) % 256;
    var rand_2 = Math.abs((Math.sin(seed++) * 10000)) % 256;
    var rand_3 = Math.abs((Math.sin(seed++) * 10000)) % 256;

    //build colour
    var red = Math.round((rand_1 + baseRed) / 2);
    var green = Math.round((rand_2 + baseGreen) / 2);
    var blue = Math.round((rand_3 + baseBlue) / 2);

    return { red: red, green: green, blue: blue };
}

const purgekey = (data) => {
	return new Promise(async (resolve, reject) => {
		var payload = _.map(data, async (items) => {
	
			var helper = `https://helper.ncdn.xyz/purgekey/`;			

			var option = {
				method: 'POST'
			}

			try {
				var result = await axios(`${ helper }${ items }`, option);
				return result.data;
			} catch(err){
				return err;
			}
			
		});	

		resolve(Promise.all(payload));

	});

}

const getLogDetails = (publisher_id, data) => {
	return new Promise(async (resolve, reject) => {
		
		var master = _.map(data.rows, async(items) => {

			var id = items;
			if(items){
				var uris = `http://52.221.245.110:4100/api/v1/log/story/${ id }`;

				var option = {
					method: 'GET',
					headers: {
						'x-pub-id': `${ publisher_id }`,
						'x-api-key': '1173e4fe-4275-4300-b53c-b8bfb7f053da',
						'Content-Type': 'application/json'
					}
				}

				var payload = await axios(uris, option);

				if(payload.status == 200){

					var obj = {}
					obj.date = payload.data.rows.date_topublish;
					obj.id = id;
					obj.user = payload.data.user;
					obj.event = payload.data.rows.submit_act;
					return obj;

				}				
			}

		});

		resolve(Promise.all(master).then(function(values) { return values.filter(function(value) { return typeof value !== 'undefined';}); }));
	})

}

module.exports = router;
