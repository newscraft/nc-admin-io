var cookieParser = require('cookie-parser');
var _ = require('underscore');
var moment = require('moment');
var express = require('express');
var router = express.Router();
var passport = require('passport');
var passportGoogle = require('../config/passport')(passport);

const { 
		getUserAll,
		getUsers, 
		getPermissions, 
		getPublishers,
		getProperty,
		getHook,
		getLogs,
		postInsert,
		postInsertExistingUser,
		postUpdate,
		updateUser,
		postInsertHook,
		postUpdateHook,
		uiNavigationSidebar
	} = require("../functions");

	const { 
		getAdmin
	} = require("../functions/api");

router.get('/', async function(req, res, next) {
	var pub_id = req.cookies['pub_id'];

	if(pub_id){
		res.redirect(302, '/d');
	}else{
		res.render('index', { title: 'Newscraft Admin', error: req.flash('error') });
	}
});

router.get('/d/:pub_id?/:pro_id?', isLoggedIn, async function(req, res) {
	const pubs = await getPublishers('');

	var pub_id = req.cookies['pub_id'];
	var pro_id = req.cookies['pro_id'];
	var properties = pubs.filter(d => d.pub_id === pub_id)

	if( req.params.pub_id && pub_id != '' && pro_id != '' ){

		pub_id = req.params.pub_id;
		pro_id = req.params.pro_id;

		res.cookie("pub_id", pub_id, { expires: new Date(Date.now() + 86400000) });
		res.cookie("pro_id", pro_id, { expires: new Date(Date.now() + 86400000) });
		res.redirect(302, '/user');

	}else{

	    res.render('dashboard', {
	        user : req.user,
	        publishers: pubs
	    });		
	}

});

router.get('/user', isLoggedIn, async function(req, res) {

	var pub_id = req.cookies['pub_id'];
	var pro_id = req.cookies['pro_id'];

	if( !pub_id ){ res.redirect(302, '/d'); return; }

	const pubs = await getPublishers(pub_id);
	const userAll = await getUserAll();
	const users = await getUsers('', pub_id, pro_id);
	const permissions = await getPermissions();
	const navigation = await uiNavigationSidebar(pub_id,pro_id,req.user)
	const admin = await getAdmin()

	res.render('user', { title : 'User Management', navs: 'user_management', admin: admin, navigation, users: users, userAll: userAll, perm: permissions });
	// var logs = await getLogs(pub_id, pro_id, user.email, 'activity');

});

router.post('/user/edit', isLoggedIn, async function(req, res) {

	var pub_id = req.cookies['pub_id'];
	var pro_id = req.cookies['pro_id'];

	var dataObj = {}
	var data = req.body;
	if( _.has(data, 'name') ){ dataObj.name = data.name; }
	if( _.has(data, 'user_id') ){ dataObj.user_id = parseInt(data.user_id); }
	if( _.has(data, 'email') ){ dataObj.email = data.email; }
	if( _.has(data, 'phone') ){ dataObj.phone_1 = data.phone; }	
	if( _.has(data, 'status') ){ dataObj.status = data.status; }	
	if( _.has(data, 'level') ){ dataObj.level = data.level; }

	const pubs = await getPublishers(pub_id);

	var datetime = moment().format('YYYY-MM-DD HH:mm:ss');

	dataObj.datetime_pub = datetime;
	dataObj.publisher_id = pub_id;
	dataObj.property_id = pro_id;
	dataObj.publisher = pubs[0].id;
	dataObj.online_browser = '';
	dataObj.online_screen = '';
	dataObj.online_screen_section = '';	

	// const resultUpdate = await postUpdate(dataObj);
	const resultUpdate = await updateUser(dataObj);
	res.redirect(`/user`);
	
});

router.post('/user/add', isLoggedIn, async function(req, res) {
	
	var pub_id = req.cookies['pub_id'];
	var pro_id = req.cookies['pro_id'];

	var dataObj = {}
	var data = req.body;
	if( _.has(data, 'name') ){ dataObj.name = data.name; }
	if( _.has(data, 'email') ){ dataObj.email = data.email; }
	if( _.has(data, 'phone') ){ dataObj.phone_1 = data.phone; }	
	if( _.has(data, 'status') ){ dataObj.status = 1; }

	const pubs = await getPublishers(pub_id);
	var pro_id_no = pubs[0].properties.filter(property => property.property_id === pro_id).map(a => a.id)[0]

	var datetime = moment().format('YYYY-MM-DD HH:mm:ss');

	dataObj.datetime_pub = datetime;
	dataObj.publisher = pubs[0].id;
	dataObj.online_browser = '';
	dataObj.online_screen = '';
	dataObj.online_screen_section = '';

	const result = postInsert(dataObj, pro_id_no);

	result.then(ok => {	
		res.json(ok)
	}).catch(err => {
		res.status(500).send(err.sqlMessage)
	})

});

router.post('/user/add_existing', isLoggedIn, async function(req, res) {
	var pub_id = req.cookies['pub_id'];
	var pro_id = req.cookies['pro_id'];
	var data = req.body;

	var obj = {}
	obj.publisher_id = pub_id
	obj.user_id = data.email_id
	obj.email = data.email
	// obj.user_id = parseInt(data.email.split('*split*')[0])
	// obj.email = data.email.split('*split*')[1]

	var pubs = await getPublishers(pub_id);
	var pro_id_no = pubs[0].properties.filter(property => property.property_id === pro_id).map(a => a.id)[0]
	
	res.cookie('selected_email', data.email)

	postInsertExistingUser(obj, pubs[0].id, pro_id_no);

	res.redirect('/user');
	
})

router.get('/webhook/:entity', isLoggedIn, async function(req, res) {

	var pub_id = req.cookies['pub_id'];
	var pro_id = req.cookies['pro_id'];
	var entity = req.params.entity;
	
	var hooks = await getHook(pub_id, pro_id, entity);
	var pubs = await getPublishers(pub_id);
	var properties = await getProperty('', pubs[0].id);
	var pubs = await getPublishers(pub_id);
	const navigation = await uiNavigationSidebar(pub_id,pro_id,req.user)

	res.render('webhook', { title: entity, navs: `webhook`, navigation, result: hooks, properties: properties });

});

router.post('/webhook/add', isLoggedIn, async function(req, res) {

	var pub_id = req.cookies['pub_id'];
	var data = req.body;

	var lang = data.property;
	var entity = data.entity;
	var pubs = await getPublishers(pub_id);
	var pro_id = await getProperty(lang, pubs[0].id);

	data.publisher_id = pub_id;
	if( _.has(data, 'status') ){ data.status = 1; }else{ data.status = 0; }
	if( _.has(data, 'property') ){ data.property_id = pro_id[0].property_id; }

	delete data.property;
	delete data.entity;

	var results = await postInsertHook(data, entity.toLowerCase());

	if(results == 'OK'){
		res.redirect(`/webhook/${ entity.toLowerCase() }?e=${ encodeURIComponent(results) }`);	
	}else{
		res.redirect(`/webhook/${ entity.toLowerCase() }?e=${ encodeURIComponent(results) }`);	
	}

});

router.post('/webhook/edit', isLoggedIn, async function(req, res) {

	var pub_id = req.cookies['pub_id'];
	var data = req.body;

	var pro_id = data.property;
	var entity = data.entity;
	var pubs = await getPublishers(pub_id);

	data.publisher_id = pub_id;
	if( _.has(data, 'status') ){ data.status = 1; }else{ data.status = 0; }
	if( _.has(data, 'property') ){ data.property_id = pro_id; }

	delete data.property;
	delete data.entity;

	var results = postUpdateHook(data, entity.toLowerCase());

	if(results == 'OK'){
		res.redirect(`/webhook/${ entity.toLowerCase() }?e=${ encodeURIComponent(results) }`);	
	}else{
		res.redirect(`/webhook/${ entity.toLowerCase() }?e=${ encodeURIComponent(results) }`);	
	}

});

router.get('/log/:entity/:key?', async function(req, res) {
	
	var pub_id = req.cookies['pub_id'];
	var pro_id = req.cookies['pro_id'];
	var entity = req.params.entity;
	var data = req.body;

	// var pro_id = data.property;
	var key = req.params.key;
	var logs = await getLogs(pub_id, pro_id, key, entity);
	var pubs = await getPublishers(pub_id);
	var navigation = await uiNavigationSidebar(pub_id,pro_id,req.user)

	// if(logs.type == 'activity'){
	let title = 'Activity'
	if(key != undefined){
		title = 'Activity : '+key
	}
	res.render('log_activity', { title, navs: 'log_story', navigation, key : key, log: logs });
	// }else{
	// 	res.render('log', { title: `${ entity }`, navs: 'log_story', navigation, log: logs });
	// }

});

router.get('/caching', async function(req, res) {

	var pub_id = req.cookies['pub_id'];
	var pro_id = req.cookies['pro_id'];
	var data = req.body;
	var pubs = await getPublishers(pub_id);
	const navigation = await uiNavigationSidebar(pub_id,pro_id,req.user)

	var pro_id = data.property;
	res.render('caching', { title: 'Caching', navs: 'caching', navigation });

});

router.get('/logout', function(req, res) {
    req.logout()
	res.clearCookie('pub_id')
	res.clearCookie('pro_id')
	res.redirect('/')
});

router.get('/auth/google', passport.authenticate('google', { 
	scope : ['profile', 'email'] 
}));

router.get('/auth/google/callback', passport.authenticate('google', {
	successRedirect : '/d',
	failureRedirect : '/',
	failureFlash : true
}));

function isLoggedIn(req, res, next) {
	
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}

module.exports = router;
