var createError = require('http-errors');
var express = require('express');
var exphbs = require('express-handlebars')
var session = require('express-session');
var path = require('path');
var cookieSession = require('cookie-session');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
// var sassMiddleware = require('node-sass-middleware');
var hbs = require('express-handlebars');
var helpers = require('handlebars-helpers');
var bodyParser = require('body-parser');
var flash=require("connect-flash");
var methodOverride = require('method-override');
var GoogleStrategy = require('passport-google');
const dotenv = require('dotenv');
dotenv.config();

var passport = require('passport');
var mysql = require('mysql');

const multihelpers  = helpers();

var app = express();

var indexRouter = require('./routes/index');
var apiRouter = require('./routes/api');

app.engine('hbs', hbs({ helpers : multihelpers, defaultLayout: false, partialsDir: ['views/partials'], extname: '.hbs' }));
// app.engine('hbs', hbs({ helpers : multihelpers, extname: '.hbs' }));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(methodOverride('X-HTTP-Method-Override'));
// app.use(session({ secret: 'supernova', saveUninitialized: true, resave: true }));
app.use(cookieSession({
    maxAge: 24 * 60 * 60 * 1000, // One day in milliseconds
    keys: ['pass']
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// app.use(sassMiddleware({
//   src: path.join(__dirname, 'public'),
//   dest: path.join(__dirname, 'public'),
//   indentedSyntax: true, // true = .sass and false = .scss
//   sourceMap: true
// }));

app.use('/', indexRouter);
app.use('/api', apiRouter);


app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res) { res.status(404).send({url: req.originalUrl + ' not found'}) });
// app.use(function(req, res, next) {
  // next(createError(404));
// });

app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');

});

// var port = process.env.PORT || 5000; //select your port or let it pull from your .env file
// app.listen(port);
console.log("listening on " + process.env.PORT + "!");

module.exports = app;