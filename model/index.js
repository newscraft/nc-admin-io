const assetHook = require('./assetHook');
const entryHook = require('./entryHook');
const admin = require('./admin');


module.exports = { assetHook, entryHook, admin }