const mongoose = require('mongoose');
const conn2 = require('../dbMongoCloud');

const adminSchemaAsset = new mongoose.Schema({
    _id: mongoose.ObjectId,
    username: String,
    email : String
},{ collection: 'users', versionKey: false });

var adminModelAsset = conn2.model('users', adminSchemaAsset);

module.exports = adminModelAsset;