const mongoose = require('mongoose');
const conn = require('../dbMongoEntry');

const webhookSchemaEntry = new mongoose.Schema({
    intent: String,
    publisher_id : String,
    property_id : String,
    url: String,
    method: String,
    status: Number,
},{ collection: 'webhook', versionKey: false });

var webhookModelEntry = conn.model('webhook', webhookSchemaEntry);

module.exports = webhookModelEntry