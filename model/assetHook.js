const mongoose = require('mongoose');
const conn2 = require('../dbMongoAsset');

const webhookSchemaAsset = new mongoose.Schema({
    intent: String,
    publisher_id : String,
    url: String,
    method: String,
    status: Number
},{ collection: 'webhook', versionKey: false });

var webhookModelAsset = conn2.model('webhook', webhookSchemaAsset);

module.exports = webhookModelAsset;