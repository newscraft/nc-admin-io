const moment = require('moment');
const _ = require('underscore');
var dbMySQL = require('../dbMySQL');

const getUserAll = async (user_mail, pub_id, key) => {
	return new Promise(async (resolve, reject) => {

		var order_by = "ORDER BY email ASC"

		try {

			//aizu20200827-temporary set WHERE to @malaysiakini.com - until September 2020
			//dbMySQL.query(`SELECT id, email FROM users WHERE status = 1 ${ order_by };`, async (err, users) => {
			dbMySQL.query(`SELECT id, email FROM users WHERE status = 1 AND email LIKE '%@malaysiakini.com%' ${ order_by };`, async (err, users) => {
				
				if(key){
					var payload = []
					payload = users.map(t => t.email)
				}else{
					var payload = {}
					payload.total = users.length
					payload.totalNotFiltered = users.length
					payload.rows = users
				}

				resolve(payload);
			});

		} catch(error) {
			reject(error);
		}

	});
}

module.exports = getUserAll;