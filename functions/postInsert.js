var dbMySQL = require('../dbMySQL');

const postInsert = async (payload, pro_id_no) => {
	return new Promise(async (resolve, reject) => {

		try {

			var sql = `INSERT INTO users SET ?`;
			dbMySQL.query(sql, payload, (err, rows) => {
				if(err) { reject(err); return; }
				
				if(rows.insertId){
					var dataPub = {}
					var dataPro = {}

					dataPub.user_id = rows.insertId
					dataPub.publisher_id = payload.publisher
					dbMySQL.query(`INSERT INTO user_to_publisher SET ?`, dataPub, (err, result) => {
						if(err) { reject(err) }
					})

					dataPro.user_id = rows.insertId
					dataPro.property_id = pro_id_no
					dataPro.permission = 0
					dbMySQL.query(`INSERT INTO user_to_property SET ?`, dataPro, (err, result) => {
						if(err) { console.log(err);  reject(err) }
					})
					resolve(rows)
				}
				
			});

		} catch(error) {
			reject(error)
		}

	});
}

module.exports = postInsert;