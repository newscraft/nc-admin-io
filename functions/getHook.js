const mongoose = require('mongoose');
const _ = require('underscore');

const getHook = async (pub_id, pro_id, entity) => {
	return new Promise(async (resolve, reject) => {

		try {

			var find = {}
			if(entity == 'asset'){ 
				var model = require('../model/assetHook');
			}else if(entity == 'story'){ 
				var model = require('../model/entryHook');
				find.property_id = pro_id
			}
			
			find.publisher_id = pub_id

			var skip = {}
			skip._id = 0

			var sort = {}
			sort._id = -1

			model
				.find(find)
				// .select(skip)
				.sort(sort)
				.lean()
				.exec(function (err, hook) {

					if( err ){ reject(err); }
				
					// if( !_.isEmpty(hook) ){

						var payload = {}
						payload.total = hook.length;
						payload.totalNotFiltered = hook.length;
						payload.rows = hook;

						resolve(payload);
						console.log(' shut '+ mongoose.connection.readyState);
						mongoose.connection.close();

					// }
				
			});	

		} catch(error) {
			reject(error);
		}

	});
}


module.exports = getHook;