const getUsers = require('./getUsers');
const getUserAll = require('./getUserAll');
const getPermissions = require('./getPermissions');
const getPublishers = require('./getPublishers');
const getProperty = require('./getProperty');
const getHook = require('./getHook');
const getLogs = require('./getLogs');
const postInsert = require('./postInsert');
const postInsertExistingUser = require('./postInsertExistingUser');
const postUpdate = require('./postUpdate');
const postInsertHook = require('./postInsertHook');
const postUpdateHook = require('./postUpdateHook');
const uiNavigationSidebar = require('./uiNavigationSidebar');
const updateUser = require('./updateUser');

module.exports = {
    getUsers,
    getUserAll,
    getPermissions,
    getPublishers,
    getProperty,
    getHook,
    getLogs,
    postInsert,
    postInsertExistingUser,
    postUpdate,
    postInsertHook,
    postUpdateHook,
    uiNavigationSidebar,
    updateUser
}