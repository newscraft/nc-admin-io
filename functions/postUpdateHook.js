const mongoose = require('mongoose');
const _ = require('underscore');

const postUpdateHook = async (payload, entity) => {
	return new Promise(async (resolve, reject) => {

		try {

			if(entity == 'asset'){ 
				var model = require('../model/assetHook');
			}else if(entity == 'story'){ 
				var model = require('../model/entryHook');
			}

			var conditions = {}
			conditions._id = payload._id;

			delete payload._id;

			model.findOneAndUpdate(
				conditions, 
				payload, 
				async(err, results) => {
					if(err){ reject(err); }
					resolve('OK')
				});

		} catch(error) {
			reject(error);
		}

	});
}


module.exports = postUpdateHook;