var dbMySQL = require('../dbMySQL');

const getPublishers = async (pub_id) => {
	return new Promise(async (resolve, reject) => {

		try {

			var query = 'SELECT id, pub_code, pub_id FROM publisher;';
			if(pub_id){ query = `SELECT id, pub_code, pub_id FROM publisher WHERE pub_id = "${ pub_id }";`; }

			dbMySQL.query(query, async (err, pubs) => {
				var payload = []
				for( var i in pubs ){
					var properties = await getProperty(pubs[i])
					payload.push(properties)
				}
				resolve(payload)
			});

		} catch(error) {
			reject(error);
			dbMySQL.on('error');
		}

	});
}

const getProperty = (data) => {
	return new Promise(async (resolve, reject) => {
		try {
			dbMySQL.query(`SELECT id, property_id, language FROM properties WHERE publisher_id = ${ data.id }`, async (err, properties) => {
				data.properties = properties
				resolve(data)
			})
		} catch(err) {
			reject(err)
		}
	})
}


module.exports = getPublishers;