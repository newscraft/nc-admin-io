const axios = require('axios');
const _ = require('underscore');
var dbMySQL = require('../dbMySQL');
var moment = require('moment');
var useragent = require('useragent');

const getLogs = async (pub_id, pro_id, key, entity) => {
	return new Promise(async (resolve, reject) => {

		try {

			if(entity == 'activity'){

				var uri = `https://hlnqv8nybj.execute-api.ap-southeast-1.amazonaws.com/prod/act`

				var params = {}
				params.pub_id = pub_id
				key ? params.user = key : ''
				var options = {
					method: 'GET',
					params: params
				}

				var result = await axios(uri, options);
				var rows = result.data ? result.data : [];

				var payload = []
				for(var i in rows){
					var time_human = ''
					moment.unix(rows[i].time, "X").isSame(moment(), 'day') ?
						time_human = moment.unix(rows[i].time, "X").utcOffset("+08:00").format("h:mm:ss A") :
						time_human = moment.unix(rows[i].time, "X").utcOffset("+08:00").format("lll")
					
					var agent = useragent.parse(rows[i].browser);

					var obj = {};
					obj.user = rows[i].user;
					obj.user_color = pastel_colour(rows[i].user);
					obj.time = moment.unix(rows[i].time, "X").format("lll");
					obj.time_human = time_human;
					obj.browser = rows[i].browser;
					obj.browser_human = agent.toString();
					obj.path = rows[i].uri;

					payload.push(obj)	 	
				}

				resolve(payload);

			}

		} catch(error) {
			reject(error);
		}

	});
}

function pastel_colour(input_str) {

    //TODO: adjust base colour values below based on theme
    var baseRed = 128;
    var baseGreen = 128;
    var baseBlue = 128;

    //lazy seeded random hack to get values from 0 - 256
    //for seed just take bitwise XOR of first two chars
    var seed = input_str.charCodeAt(0) ^ input_str.charCodeAt(1);
    var rand_1 = Math.abs((Math.sin(seed++) * 10000)) % 256;
    var rand_2 = Math.abs((Math.sin(seed++) * 10000)) % 256;
    var rand_3 = Math.abs((Math.sin(seed++) * 10000)) % 256;

    //build colour
    var red = Math.round((rand_1 + baseRed) / 2);
    var green = Math.round((rand_2 + baseGreen) / 2);
    var blue = Math.round((rand_3 + baseBlue) / 2);

    return { red: red, green: green, blue: blue };
}

module.exports = getLogs;