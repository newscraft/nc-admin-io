const mongoose = require('mongoose');
const _ = require('underscore');

const postInsertHook = async (payload, entity) => {
	return new Promise(async (resolve, reject) => {

		try {

			if(entity == 'asset'){ 
				var model = require('../model/assetHook');
			}else if(entity == 'story'){ 
				var model = require('../model/entryHook');
			}

			var find = {}
			find.url = payload.url;
			find.publisher_id = payload.publisher_id;
			find.property_id = payload.property_id;
			find.status = 1;

			model
				.findOne(find)
				.lean()
				.exec( async(err, results) => {
					if(err){ reject(err); }
					
					if( _.isEmpty(results) ){
						var response = await doAdd(model, payload);
					}else{
						var response = 'Failed';
					}

					resolve(response);

				});

		} catch(error) {
			reject(error);
		}

	});
}

const doAdd = (model, payload) => {
	return new Promise(async (resolve, reject) => {
		var data1 = new model(payload);
		data1.save( (err, response) => {
			if(err){ reject(err); }
			resolve('OK');
		});
	});

}

module.exports = postInsertHook;