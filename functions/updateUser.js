var dbMySQL = require('../dbMySQL');
const getPublishers = require("./getPublishers");

const updateUser = async (payload) => {
	return new Promise(async (resolve, reject) => {

		try {

			var updateUser = {}
			updateUser.name = payload.name;
			updateUser.phone_1 = payload.phone_1;
			// updateUser.status = payload.status;

			var query = dbMySQL.query(`UPDATE users SET ? WHERE id = ${ payload.user_id }`, updateUser, async (err, rows) => {
				if(err) { console.log(err); reject(err); }

				// if(payload.level != undefined){
					var d = await userPermission(payload);
				// }
				resolve('OK');
				
			});

		} catch(error) {
			reject(error);
		}

	});
}

const userPermission = (payload) => {
	return new Promise(async (resolve, reject) => {
        
        var publishers = await getPublishers(payload.publisher_id)
        var pro_id_no = publishers[0].properties.filter(property => property.property_id === payload.property_id).map(a => a.id)[0]

        var levels = payload.level;
        
        dbMySQL.query(`DELETE FROM user_to_property WHERE user_id = ${ payload.user_id } AND property_id = '${ pro_id_no }'`, (err, value) => {
            if(err){ reject(err); }
			if(payload.level != undefined){
				for(var i in levels){

					var updateLevel = {}
					updateLevel.status = payload.status;
					updateLevel.user_id = payload.user_id;
					updateLevel.property_id = pro_id_no;
					updateLevel.permission = parseInt(levels[i]);

					var sql = `REPLACE INTO user_to_property SET ?`;
					dbMySQL.query(sql, updateLevel, async (err, rows) => {
						if(err) { reject(err); }
						resolve(rows)
					});
				}
			}else{
				var updateLevel = {}
				updateLevel.status = payload.status;
				updateLevel.user_id = payload.user_id;
				updateLevel.property_id = pro_id_no;
				updateLevel.permission = 0;

				var sql = `REPLACE INTO user_to_property SET ?`;
				dbMySQL.query(sql, updateLevel, async (err, rows) => {
					if(err) { reject(err); }
					resolve(rows)
				});				
			}
        })

		dbMySQL.query(`DELETE FROM user_permissions WHERE user_id = ${ payload.user_id } AND publisher_id = '${ payload.publisher_id }'`, (err, value) => {
			if(err){ reject(err); }

			if(payload.level != undefined){
				for(var i in levels){

					var updateLevel = {}
					updateLevel.user_id = payload.user_id;
					updateLevel.email = payload.email;
					updateLevel.publisher_id = payload.publisher_id;
					updateLevel.permission = parseInt(levels[i]);

					var sql = `REPLACE INTO user_permissions SET ?`;
					var query = dbMySQL.query(sql, updateLevel, async (err, rows) => {
						if(err) { reject(err); }
						resolve(rows)
					});

				}
			}else{
				var updateLevel = {}
				updateLevel.user_id = payload.user_id;
				updateLevel.email = payload.email;
				updateLevel.publisher_id = payload.publisher_id;
				updateLevel.permission = 0;

				var sql = `REPLACE INTO user_permissions SET ?`;
				var query = dbMySQL.query(sql, updateLevel, async (err, rows) => {
					if(err) { reject(err); }
					resolve(rows)
				});				
			}

		});

	});

}

module.exports = updateUser;