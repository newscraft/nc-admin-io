var dbMySQL = require('../dbMySQL');

const postUpdate = async (payload) => {
	return new Promise(async (resolve, reject) => {

		try {

			var updateUser = {}
			updateUser.name = payload.name;
			updateUser.phone_1 = payload.phone_1;
			updateUser.status = payload.status;

			var query = dbMySQL.query(`UPDATE users SET ? WHERE id = ${ payload.user_id }`, updateUser, async (err, rows) => {
				if(err) { console.log(err); reject(err); }

				if(payload.level != undefined){
					var d = await userPermission(payload);
				}
				resolve('OK');
				
			});

		} catch(error) {
			reject(error);
		}

	});
}

const userPermission = (payload) => {
	return new Promise(async (resolve, reject) => {

		var levels = payload.level;

		dbMySQL.query(`DELETE FROM user_permissions WHERE user_id = ${ payload.user_id } AND publisher_id = '${ payload.publisher_id }'`, (err, value) => {
			if(err){ reject(err); }

			for(var i in levels){

				var updateLevel = {}
				updateLevel.user_id = payload.user_id;
				updateLevel.email = payload.email;
				updateLevel.publisher_id = payload.publisher_id;
				updateLevel.permission = parseInt(levels[i]);

				var sql = `REPLACE INTO user_permissions SET ?`;
				var query = dbMySQL.query(sql, updateLevel, async (err, rows) => {
					if(err) { reject(err); }
					resolve(rows)
				});

			}

		});

	});

}

module.exports = postUpdate;