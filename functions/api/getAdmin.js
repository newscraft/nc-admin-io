const moment = require('moment')
const _ = require('underscore')

const Admin = require('../../model/admin');

const getAdmin = (id) => {
    return new Promise(async (resolve, reject) => {
        var params = {}
        id ? params._id = id : ''

        try {
            await Admin.find(params, (err, items) => {
                if (err){
                    reject(err)
                }else{
                    var result = items.map(item => item.email)
                    resolve(result)
                }
            });            
        } catch (error) {
            reject(error)
        }

    })
}

module.exports = getAdmin;