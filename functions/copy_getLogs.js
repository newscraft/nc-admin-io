const axios = require('axios');
const _ = require('underscore');
var dbMySQL = require('../dbMySQL');
var moment = require('moment');
var useragent = require('useragent');

const getLogs = async (pub_id, pro_id, key, entity) => {
	return new Promise(async (resolve, reject) => {

		try {

			if(entity == 'activity'){

				var where = ''
				if(key != undefined){
					where += ` AND user = '${key}'`
				}

				dbMySQL.query(`SELECT * FROM user_activity WHERE user != '' ${where} ORDER BY time DESC LIMIT 0,500;`, async (err, rows) => {

					var payload = [];
					for(var i in rows){

						time_human = ''
						if(moment.unix(rows[i].time, "X").isSame(moment(), 'day')){
							time_human = moment.unix(rows[i].time, "X").utcOffset("+08:00").format("h:mm:ss A")
						}else{
							time_human = moment.unix(rows[i].time, "X").utcOffset("+08:00").format("lll")
						}

						var agent = useragent.parse(rows[i].browser); // 'Chrome 15.0.874 / Mac OS X 10.8.1'

						var obj = {};
						obj.user = rows[i].user;
						obj.user_color = pastel_colour(rows[i].user);
						obj.time = moment.unix(rows[i].time, "X").format("lll");
						obj.time_human = time_human;
						obj.browser = rows[i].browser;
						obj.browser_human = agent.toString();
						obj.path = rows[i].uri;

						payload.push(obj);

					}
					var pl_list = {
						'key' : key,
						'type' : 'activity',
						'data' : {
							'rows' : payload
						}
					}
					resolve(pl_list);
				})


			}else{

				if( !key ){ 
					var uri = `http://localhost:5000/api/log/${ entity }`; 
				}else{
					var uri = `http://localhost:5000/api/log/${ entity }/${ key }`;
				}

				var option = {
					method: 'GET',
					headers: { 
						'x-pub-id': `${ pub_id }`, 
						'x-property-id': `${ pro_id }`,
						'x-api-key': '1173e4fe-4275-4300-b53c-b8bfb7f053da', 
					}
				}

				var result = await axios(uri, option);
				resolve(result);

			}

		} catch(error) {
			reject(error);
		}

	});
}

function pastel_colour(input_str) {

    //TODO: adjust base colour values below based on theme
    var baseRed = 128;
    var baseGreen = 128;
    var baseBlue = 128;

    //lazy seeded random hack to get values from 0 - 256
    //for seed just take bitwise XOR of first two chars
    var seed = input_str.charCodeAt(0) ^ input_str.charCodeAt(1);
    var rand_1 = Math.abs((Math.sin(seed++) * 10000)) % 256;
    var rand_2 = Math.abs((Math.sin(seed++) * 10000)) % 256;
    var rand_3 = Math.abs((Math.sin(seed++) * 10000)) % 256;

    //build colour
    var red = Math.round((rand_1 + baseRed) / 2);
    var green = Math.round((rand_2 + baseGreen) / 2);
    var blue = Math.round((rand_3 + baseBlue) / 2);

    return { red: red, green: green, blue: blue };
}

module.exports = getLogs;