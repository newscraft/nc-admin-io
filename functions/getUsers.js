const moment = require('moment');
const _ = require('underscore');
var dbMySQL = require('../dbMySQL');

const getProperty = require("./getProperty");
const getPublishers = require("./getPublishers");

const getUsers = async (user_mail, pub_id, pro_id) => {
	return new Promise(async (resolve, reject) => {

		var order_by = "ORDER BY last_online DESC"

		try {

			var publishers = await getPublishers(pub_id)
			var pub_id_no = publishers[0].id
			var pro_id_no = publishers[0].properties.filter(property => property.property_id === pro_id).map(a => a.id)[0]
			
			var query = `SELECT * FROM user_to_publisher WHERE publisher_id = ${ pub_id_no }`
			dbMySQL.query(query, async (err, pubs) => {
				if(err) { reject(err) }
				var data = []
				for( var i in pubs ){
					var pub = pubs[i]
					var properties = await listProperty(pub.user_id, pro_id_no)
					var status_by_property = await statusProperty(pub.user_id, pro_id_no)
					if(properties.length >= 1){
						var details = await listUserDetails(pub.user_id)

						if(details[0]){
							// pub.status = details[0].status ? details[0].status : 0
							pub.status = status_by_property
							pub.email = details[0].email
							pub.avatar = details[0].avatar
							pub.name = details[0].name
							pub.last_login = moment.unix(details[0].last_login).format("MM-DD-YYYY")
							// pub.last_online_human = moment.unix(details[0].last_online).fromNow();
							if(details[0].last_online != 0){
								pub.last_online_human = moment.unix(details[0].last_online).fromNow();	
							}else{
								pub.last_online_human = 0
							}								
							
							pub.last_online = details[0].last_online
							pub.phone_1 = details[0].phone_1
						}
						pub.level = properties	
						data.push(pub)						
					}
				}

				var master_payload = {}
				master_payload.total = pubs.length
				master_payload.totalNotFiltered = pubs.length
				master_payload.rows = data.sort((a, b) => b.last_online - a.last_online)

				resolve(master_payload)
			})

			// dbMySQL.query(`SELECT id FROM publisher WHERE pub_id = '${ pub_id }';`, async (err, pubs) => {

			// 	var pub_id_no = pubs[0].id
			// 	var pro_id_no = await getProperty('', pub_id_no, pro_id)
			// 	pro_id_no = pro_id_no[0].id

			// 	if(user_mail){
			// 		var sql = `SELECT user_id, permission 
			// 		FROM user_permissions WHERE publisher_id = "${ pub_id }" AND email = "${ user_mail }";`
			// 	}else{
			// 		var sql = `SELECT user_id, permission 
			// 		FROM user_permissions WHERE publisher_id = "${ pub_id }";`;
			// 	}
				
			// 	dbMySQL.query(sql, async (err, rows) => {
			// 		if(err) { reject(err) }
					
			// 		var grouped = _.mapObject(_.groupBy(rows, 'user_id'), ulist => ulist.map(row => _.omit(row, 'user_id')))

			// 		var data = []
			// 		for(var i in grouped){
			// 			var payload = await getUserDetails(i, grouped[i])
			// 			data.push(payload)
			// 		}
			// 		var master_payload = {}
			// 		master_payload.total = rows.length
			// 		master_payload.totalNotFiltered = rows.length
			// 		master_payload.rows = data

			// 		resolve(master_payload);

			// 	})
			// });


		} catch(error) {
			reject(error);
		}

	});
}

const listUserDetails = (user_id) => {
	return new Promise((resolve, reject) => {
		var order_by = "ORDER BY last_online DESC"
		var query = `SELECT status, email, avatar, name, last_login, last_online, phone_1 FROM users WHERE id = ${ user_id } ${ order_by };`;

		try {
			dbMySQL.query(query, (err, result) => {
				if(err) { reject(err); }
				if(result[0]){
					var details = result;
				}else{
					var details = [];
				}
				resolve(details)
			})
		} catch (error) {
			reject(error)
		}
	})
}

const listProperty = (user_id, pro_id_no) => {
	return new Promise((resolve, reject) => {
		var query = `SELECT status, property_id, permission FROM user_to_property WHERE user_id = ${ user_id } AND property_id = ${ pro_id_no };`;
		try {
			dbMySQL.query(query, (err, result) => {
				if(err) { reject(err); }
				if(result[0]){
					var perm = result;
				}else{
					var perm = [];
				}
				var data = JSON.parse(JSON.stringify(perm));
				var level = []
				for(var i in data){
					level.push(data[i].permission);
				}
				resolve(level);
			})			
		} catch (error) {
			reject(error)
		}
	})
}

const statusProperty = (user_id, pro_id_no) => {
	return new Promise((resolve, reject) => {
		var query = `SELECT DISTINCT status FROM user_to_property WHERE user_id = ${ user_id } AND property_id = ${ pro_id_no };`;
		try {
			dbMySQL.query(query, (err, result) => {
				if(err) { reject(err); }
				if(result[0]){
					var status = result[0].status;
				}else{
					var status = 0;
				}
				resolve(status);
			})
		} catch (error) {
			reject(error)
		}
	})
}

const getUserDetails = (id, group) => {
	return new Promise((resolve, reject) => {
		var sql = `SELECT id, publisher, email, name, datetime_pub, phone_1, last_login, last_online, status FROM users WHERE id = ${ id };`;
		dbMySQL.query(sql, async (err, result) => {
			if(err) { reject(err); }
			
			if(!_.isEmpty(result)){
				for(var i in result){
					result[i]['user_id'] = result[i].id
					result[i]['last_login'] = moment.unix(result[i].last_login).format("MM-DD-YYYY");
					result[i]['level'] = group.map(a => a.permission);

					if(result[i]['last_online'] != 0){
						result[i]['last_online_human'] = moment.unix(result[i].last_online).fromNow();	
					}else{
						result[i]['last_online_human'] = 0
					}
					resolve(result[i])
				}
			}else{
				resolve(null)
			}

		})
	})
}

const userPerm = (pub_id, user_id, obj) => {
	return new Promise(async (resolve, reject) => {

		try {

			dbMySQL.query(`SELECT publisher_id, property_id, default_property, permission FROM user_permissions WHERE user_id = ${ user_id } AND publisher_id = "${ pub_id }";`, (err, result) => {
				if(err) { reject(err); }

				if(result[0]){
					var perm = result;
				}else{
					var perm = [];
				}
				var data = JSON.parse(JSON.stringify(perm));
				var level = []
				for(var i in data){
					// data[] = data[i].permission;
					level.push(data[i].permission);
				}
				
				obj.level = level
				
				resolve(obj);

			});

		} catch(err) {
			reject(err);
		}

	});
}

module.exports = getUsers;