const mongoose = require('mongoose');
const _ = require('underscore');

const getPublishers = require('./getPublishers');
const { prependListener } = require('process');

const uiNavigationSidebar = async (pub_id,pro_id,admin) => {
	return new Promise(async (resolve, reject) => {

		let pubs = await getPublishers(pub_id);

		let pro_dic = {}
		pubs[0].properties.map(item => {
			pro_dic[item.property_id] = item
		})
		// console.log('pro_dic',pro_dic)		
		// console.log('pubs',pubs)
		// console.log('pro_id',pro_id)

		let current_prop = `${pubs[0].pub_code} - ${pro_dic[pro_id].language}`
		let allow_access = (admin === 'Aliff Azmi' || admin === 'Aizu Ikmal') ? 1 : 0

		resolve({allow_access,current_prop,publisher:pubs})

	});
}

module.exports = uiNavigationSidebar;