var dbMySQL = require('../dbMySQL');
var lowerCase = require('lower-case');

const getProperty = async (lang, pubID, proID) => {
	return new Promise(async (resolve, reject) => {

		try {

			if(lang){ var language = lowerCase(lang); var syntax = `property_id = '${ lang }' AND`; }else{ var syntax = '' }
			if(proID){ var syntax = `property_id = '${ proID }' AND` }else{ var syntax = '' }
			
			var query = `SELECT id, property_id, language FROM properties WHERE ${ syntax } publisher_id = ${ pubID };`;
			
			dbMySQL.query(query, (err, pros) => {
				resolve(pros);
			});

		} catch(error) {
			reject(error);
		}

	});
}


module.exports = getProperty;