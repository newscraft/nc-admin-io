var dbMySQL = require('../dbMySQL');

const getPermissions = async () => {
	return new Promise(async (resolve, reject) => {

		try {
			dbMySQL.query('SELECT * FROM `permissions` ORDER BY name ASC;', (err, rows) => {
				if(err) { reject(err); }
				resolve(rows);
			});
		} catch(error) {
			reject(error);
		}

	});
}

module.exports = getPermissions;