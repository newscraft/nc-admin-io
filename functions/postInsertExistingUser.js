var dbMySQL = require('../dbMySQL');

const postInsertExistingUser = async (payload, pub_id, pro_id_no) => {
	return new Promise(async (resolve, reject) => {

		try {
			
			dbMySQL.query(`DELETE FROM user_permissions WHERE user_id = ${ payload.user_id } AND publisher_id = '${ payload.publisher_id }'`, (err, value) => {
				if(err){ reject(err); }

				var sql = `INSERT INTO user_permissions SET ?`;
				dbMySQL.query(sql, payload, (err, rows) => {
					if(err) { console.log(err); reject(err); }
					resolve(rows);
				});

				var d = {}
				var e = {}
				d.user_id = payload.user_id
				d.publisher_id = pub_id

				dbMySQL.query(`INSERT INTO user_to_publisher SET ?`, d, (err, rows) => {
					if(err) { console.log(err); reject(err); }
					resolve(rows);
				});

				e.user_id = payload.user_id
				e.property_id = pro_id_no
				e.permission = 0

				dbMySQL.query(`INSERT INTO user_to_property SET ?`, e, (err, rows) => {
					if(err) { console.log(err); reject(err); }
					resolve(rows);
				});	

			})

			resolve('ok')

		} catch(error) {
			reject(error);
		}

	});
}

module.exports = postInsertExistingUser